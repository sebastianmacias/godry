package godry

import (
	//"bytes"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"math"

	uuid "github.com/satori/go.uuid"

	log "github.com/sirupsen/logrus"

	//"golang.org/x/text/transform"
	//	"golang.org/x/text/unicode/norm"
	"crypto/aes"
	"crypto/cipher"
	c_rand "crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// GetMapStringValue ...
func GetMapStringValue(dict map[string]string) (string, error) {

	if val, ok := dict["foo"]; ok {
		return val, nil
	}

	return "", errors.New("Error getting map string value")
}

// GetHostFromURL ...
func GetHostFromURL(s string) string {

	u, err := url.Parse(s)
	if err != nil {
		return ""
	}

	host := StringBefore(u.Host, ":")

	return host
}

func StringPanicIfEmpty(text *string) {
	if len(StringTrim(*text)) == 0 {
		panic("Empty String")
	}
}

// AnyStringIsNotEmpty

func SomeStringsAreNotEmpty(textValues ...string) bool {
	for _, t := range textValues { // Iterates over the arguments whatever the number.
		if StringIsNotEmpty(t) {
			return true
		}
	}

	return false
}

func AllStringsAreEmpty(textValues ...string) bool {

	for _, t := range textValues { // Iterates over the arguments whatever the number.
		if StringIsNotEmpty(t) {
			return false
		}
	}

	return true
}

// GetURLHost returns the hostname of a given URL
func GetURLHost(URL string) (string, error) {

	u, err := url.Parse(URL)

	if err != nil {
		return "", err
	}

	return u.Host, nil
}

// RemoveQueryParametersFromURL removes specified query params from URL
func RemoveQueryParametersFromURL(URL string, params ...string) (*url.URL, error) {

	u, err := url.Parse(URL)

	if err != nil {
		return nil, err
	}

	q, err := url.ParseQuery(u.RawQuery)

	if err != nil {
		return nil, err
	}

	for _, p := range params { // Iterates over the arguments whatever the number.
		q.Del(p)
	}

	u.RawQuery = q.Encode()

	return u, nil
}

// GetURLParam returns an URL param
func GetURLParam(URL, param string) (string, error) {

	u, err := url.Parse(URL)

	if err != nil {
		return "", err
	}

	q, err := url.ParseQuery(u.RawQuery)

	if err != nil {
		return "", err
	}

	return q[param][0], nil
}

// StringIsEmpty returns true if provided string is empty
func StringIsEmpty(text string) bool {
	if len(StringTrim(text)) == 0 {
		return true
	}
	return false
}

// JoinStringSlice returns a string based on slice using sep as separator
func JoinStringSlice(strings []string, sep string) string {

	var returnStr = ""
	var useSep = ""
	for _, s := range strings {
		returnStr = returnStr + useSep + s
		useSep = sep
	}

	return returnStr

}

// StringIsNotEmpty returns true if provided string is NOT empty
func StringIsNotEmpty(text string) bool {

	return !StringIsEmpty(text)
}

func StringSetIfEmpty(text string, replaceWith string) string {

	text = StringTrim(text)
	if StringIsEmpty(text) {
		text = replaceWith
	}

	return text
}

func ForceSetTimeToUTC(t time.Time) (time.Time, error) {

	timeString := t.String()

	timeString = GetStringBefore(timeString, " ") + " UTC"

	UTMTime, err := time.Parse("2006-01-02 15:04:05.999999999 -0700 MST", timeString)

	return UTMTime, err

}

func GetStringBefore(s, sub string) string {
	stringParts := StringSplit(s, sub)

	return strings.Join(stringParts[:len(stringParts)-1], sub)
}

func DateToInts(date time.Time) (int, int, int) {
	return date.Day(), int(date.Month()), date.Year()
}

func DateToStrings(date time.Time) (string, string, string) {
	day, month, year := DateToInts(date)

	dayStr := IntToString(day)
	monthStr := IntToString(month)
	yearStr := IntToString(year)

	if len(dayStr) == 1 {
		dayStr = "0" + dayStr
	}

	if len(monthStr) == 1 {
		monthStr = "0" + monthStr
	}

	return dayStr, monthStr, yearStr
}

//JSONStringToStruct converts jsonString to struct
func JSONStringToStruct(jsonString string, v interface{}) {
	json.Unmarshal([]byte(jsonString), v)
}

func JSONBytesToStruct(jsonBytes []byte, v interface{}) {
	json.Unmarshal(jsonBytes, v)
}

func URLToStruct(url string, v interface{}) {

	jsonBytes := UrlToByte(url)

	JSONBytesToStruct(jsonBytes, &v)
}

func StructToJsonBytes(obj interface{}) []byte {
	objB, _ := json.Marshal(obj)
	return objB
}

func StructToJsonString(obj interface{}) string {
	return string(StructToJsonBytes(obj))
}

func JsonBytesToFile(jsonBytes []byte, path string) error {
	var obj interface{}
	JSONBytesToStruct(jsonBytes, obj)
	err := ioutil.WriteFile(path, jsonBytes, 0644)

	if err != nil {
		return err
	}
	return nil
}

func StructToMd5(obj interface{}) string {
	// str := StructToJsonString(obj)
	md5, _ := StringToMd5Hash(StructToJsonString(obj))

	return md5
}

func StringToInt(s string) int {
	i, err := strconv.Atoi(s)

	if err != nil {
		return 0
	}

	return i
}

func StringToInt64(s string) int64 {

	i, err := strconv.ParseInt(s, 10, 64)

	if err != nil {
		return 0
	}

	return i
}

func StringToBytes(s string) []byte {
	return []byte(s)
}

func StringSplit(s, sub string) []string {
	string_array := strings.Split(s, sub)
	return string_array
}

func StringBefore(s, sub string) string {
	string_array := strings.Split(s, sub)
	return string_array[0]
}

func StringArrayToIntArray(strArray []string) []int {
	var t2 = []int{}

	for _, i := range strArray {
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		t2 = append(t2, j)
	}
	return t2
}

func StringSplitAndTrim(s, sub string) []string {
	var returnStringSlice []string

	stringSlice := StringSplit(s, sub)
	for _, item := range stringSlice {

		item = StringTrim(item)
		if len(item) > 0 {
			returnStringSlice = append(returnStringSlice, item)
		}
	}
	return returnStringSlice

}

func StringCount(s, sep string) int {
	return strings.Count(s, sep)
}

func StringToFloat32(s string) (float32, error) {
	f, err := strconv.ParseFloat(s, 32)

	if err != nil {
		return 0, err
	}

	return float32(f), nil
}

func StringToFloat64(s string) (float64, error) {
	f, err := strconv.ParseFloat(s, 64)

	if err != nil {
		return 0, err
	}

	return f, nil
}

func StringToTime(s string, f string) (time.Time, error) {
	t, err := time.Parse(f, s)

	if err != nil {
		var et time.Time
		return et, err
	}

	return t, nil
}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func Float32ToFixed(num float32, precision int) float32 {

	return float32(Float64ToFixed(float64(num), precision))
}

func Float64ToFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

func StringRemoveSpaces(s string) string {
	return StringRemove(s, " ")
}

func StringRemove(s, sep string) string {
	return StringReplace(s, sep, "")
}

func StringReplace(s, old, new string) string {
	return strings.Replace(s, old, new, -1)
}

func StringTimeToFloat32(s string) float32 {

	if len(s) == 8 {
		s = StringReplace(s, ":00", "")
	}

	f, _ := StringToFloat32(StringReplace(s, ":", "."))

	return f
}

func StringToMd5Hash(s string) (string, error) {
	hasher := md5.New()
	hasher.Write([]byte(s))
	return hex.EncodeToString(hasher.Sum(nil)), nil
}

func StringTrim(s string) string {

	return strings.Trim(strings.TrimSpace(strings.Trim(s, "	")), " ")
}

func IntToString(i int) string {
	s := strconv.Itoa(i)
	return s
}

func IntToFloat32(i int) float32 {
	return float32(i)
}

func IntToFloat64(i int) float64 {
	return float64(i)
}

func Int64ToString(i int64) string {
	s := strconv.FormatInt(i, 10)
	return s
}

func StringToUpper(s string) string {
	return strings.ToUpper(s)
}

func StringToLower(s string) string {
	return strings.ToLower(s)
}

func StringContains(s, c string) bool {
	return strings.Contains(s, c)
}

func StringStartsWith(s, c string) bool {
	return strings.HasPrefix(s, c)
}

// func StringRemovePrefix(s, p string) string{
// 	if
// }

func StringContainsCaseInsensitive(s, c string) bool {

	s = strings.ToLower(s)
	c = strings.ToLower(c)

	return strings.Contains(s, c)
}

// StringIsInSlice returns true if string is found in the slice
func StringIsInSlice(text string, list []string) bool {
	for _, b := range list {
		if b == text {
			return true
		}
	}
	return false
}

// StringArrayContains DEPRECATED
func StringArrayContains(list []string, a string) bool {
	return StringIsInSlice(a, list)
}

func BoolToString(b bool) string {
	if b {
		return "true"
	}

	return "false"
}

func EncodeURI(s string) string {
	return url.QueryEscape(s)
}

func DencodeURI(s string) string {
	rs, _ := url.QueryUnescape(s)
	return rs
}

func DateToStringFormat1(d time.Time) string {
	return d.Format("02-01-2006")
}

func Float32ToString(f float32) string {

	var sResult1 string = fmt.Sprintf("%.2f", f)

	return sResult1
}

func Float64ToString(f float64) string {
	// to convert a float number to a string
	return strconv.FormatFloat(f, 'f', 6, 64)
}

func ShrinkFloat64(f float64) (float64, error) {
	fStr := Float64ToString(f)
	parts := StringSplit(fStr, ".")

	decimalCount := len(parts[1])

	if decimalCount < 2 {
		return f, errors.New("can't shrink")
	}

	newDecimalCount := decimalCount - 1

	newF := Float64ToFixed(f, newDecimalCount)

	return newF, nil

}

func RandomFloat32Number(min, max float32) float32 {
	return rand.Float32()*(max-min) + min
}

func RandomUpperStringAndNumbers(lenght int) string {
	var letterRunes = []rune("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, lenght)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func RandomUpperString(lenght int) string {
	var letterRunes = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, lenght)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func RandomLowerString(lenght int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyz")
	b := make([]rune, lenght)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func RandomString(lenght int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, lenght)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func GenerateUUID() string {
	u, _ := uuid.NewV4()

	return u.String()

}

func DeleteFile(path string) {
	// delete file
	var err = os.Remove(path)

	if err != nil {
		fmt.Printf("%s", err)
	}
}

func UrlToByte(url string) []byte {
	var contents []byte
	var err error

	response, err := http.Get(url)
	if err != nil {
		fmt.Printf("%s", err)
	} else {
		defer response.Body.Close()
		contents, err = ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
		}

	}

	return contents

}

func UrlToString(url string) string {

	return StringTrim(string(UrlToByte(url)))

}

func FileExists(filePath string) bool {
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return false
	}
	return true
}

func DirExists(dirPath string) bool {
	if _, err := os.Stat(dirPath); os.IsNotExist(err) {
		return false
	}
	return true
}

func CreateDirIfNotExists(dirPath string) bool {
	if DirExists(dirPath) {
		return false
	} else {

		err := os.MkdirAll(dirPath, 0777)
		if err != nil {
			panic(err)
		}
		return true

	}

}

func StripCtlAndExtFromUnicode(str string) string {
	return strings.Map(func(r rune) rune {
		if r >= 32 && r < 127 {
			return r
		}
		return -1
	}, str)
}

//Channels

func WaitForChannelsToClose(chans ...chan struct{}) {
	//Taken from and docs: https://gist.github.com/montanaflynn/d9b358249939c5c541ec

	t := time.Now()
	for _, v := range chans {
		<-v
		log.Debug(string(time.Since(t)) + "for chan to close ")

	}
	log.Debug(string(time.Since(t)) + "for channels to close ")
}

func CloseChannels(chans []chan struct{}) {

	for _, c := range chans {
		fmt.Printf("godry.CloseChannels c: ", c)
		close(c)
	}
}

//Encryption
// encrypt string to base64 crypto using AES
func EncryptAES(key, text string) string {
	keyBytes := []byte(key)
	plaintext := []byte(text)

	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(c_rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// convert to base64
	return base64.URLEncoding.EncodeToString(ciphertext)
}

// decrypt from base64 to decrypted string
func DecryptAES(key string, cryptoText string) string {

	keyBytes := []byte(key)

	ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		panic("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)

	return fmt.Sprintf("%s", ciphertext)
}
